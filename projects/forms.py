from django.forms import ModelForm
from projects.models import Project


# create models for PROJECTS
class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = [
            "name",
            "description",
            "owner",
        ]
