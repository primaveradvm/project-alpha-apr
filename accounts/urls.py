from django.urls import path
from accounts.views import user_login, user_logout, create_account

# urls.py ACCOUNTS!!!!
urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", create_account, name="signup"),
]
