from django.forms import ModelForm
from tasks.models import Task


# forms for TASKS TASKS TASKS TASKS
class TaskForm(ModelForm):
    class Meta:
        model = Task
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
